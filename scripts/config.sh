# Copyright 2016 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
function var_type()
{
    case "$(declare -p "$1" 2>/dev/null | head -n 1 | cut -d ' ' -f 2)" in
        --|-x)
            [ "$2" = scalar ]
            ;;
        -a|-ax)
            [ "$2" = array ]
            ;;
        *)
            false
            ;;
    esac
}

function declare_array()
{
    local name="$1"
    shift

    if var_type "$name" scalar
    then
        read -r -d $'\0' -a "$name" <<< "${!name}"
    elif ! var_type "$name" array
    then
        eval "$name=(\$@)"
    fi
}

: ${ROOT="$(dirname "$SCRIPTS_PATH")"}

: ${DATA_PATH="$ROOT/data"}
: ${GRAPHS_PATH="$DATA_PATH/graphs"}
: ${RAW_TRANSCRIPT_PATH="$DATA_PATH/transcripts/raw"}
: ${PARSED_TRANSCRIPT_PATH="$DATA_PATH/transcripts/parsed"}
: ${LINES_PATH="$DATA_PATH/lines"}
: ${GRAPHVIZ_PATH="$DATA_PATH/dot"}

: ${EPISODE_LIST_FILE="$DATA_PATH/episodes"}

: ${BUILD_PATH="$ROOT/build"}
: ${GENERATOR="$BUILD_PATH/bin/graphgen"}


declare_array SEASONS 1 2 3 4 5 6

IFS=$'\n' declare_array CHARACTERS \
        "Apple Bloom" \
        "Applejack" \
        "Big McIntosh" \
        "Discord" \
        "Fluttershy" \
        "Pinkie Pie" \
        "Princess Celestia" \
        "Princess Luna" \
        "Rainbow Dash" \
        "Rarity" \
        "Scootaloo" \
        "Starlight Glimmer" \
        "Sweetie Belle" \
        "Twilight Sparkle" \
        "Spike"
