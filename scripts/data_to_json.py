#!/usr/bin/python
# Copyright 2016 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from __future__ import print_function
import os
import sys
import json
import urllib
from wikitranscript import lines


class JsonGenerator(list):
    """
    Hack to make json.dump work on a generator
    """
    def __init__(self, generator):
        self.generator = generator

    def __iter__(self):
        return self.generator

    def __len__(self):
        return 1


def dump_episode(filename):
    """
    Dumps an episode from its file name
    """
    with open(filename) as file:
        slug = os.path.basename(filename)
        json.dump(
            {
                "slug": slug,
                "title": urllib.unquote(slug.replace("_", " "))
                         .replace(" (episode)", ""),
                "lines": JsonGenerator(
                    {"characters": characters, "text": text}
                    for characters, text in lines(file)
                )
            },
            sys.stdout,
            indent=4
        )


print("[")
if len(sys.argv) > 1:
    for filename in sys.argv[1:-1]:
        dump_episode(filename)
        print(",")
    dump_episode(sys.argv[-1])
    print("")
print("]")
