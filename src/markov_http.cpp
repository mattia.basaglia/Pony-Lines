/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright 2016 Mattia Basaglia
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <fstream>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>

#include "melanolib/string/text_generator.hpp"
#include "httpony.hpp"

struct MarkovChains
{
    void load(const std::string& path)
    {
        using namespace boost::filesystem;
        if ( is_directory(path) )
        {
            std::cout << "Scanning " << path << '\n';
            for ( const auto& item : boost::make_iterator_range(directory_iterator(path), {}) )
                load(item.path().string());
        }
        else if ( is_regular(path) )
        {
            std::cout << "Loading " << path << '\n';
            std::ifstream input(path);
            if ( input.is_open() )
                generators[basename(path)].load(input);
        }
        else
        {
            std::cout << "Skipping " << path << '\n';
        }
    }

    std::string generate(
        const std::string& chain,
        const std::string& prompt,
        std::size_t min_words,
        std::size_t enough_words) const
    {
        if ( generators.empty() )
            return "";

        auto iter = generators.find(chain);
        if ( iter == generators.end() )
        {
            iter = generators.begin();
            std::advance(iter, melanolib::math::random(0, generators.size() - 1));
        }

        return iter->second.generate_string(
            prompt,
            melanolib::math::min(min_words, max_words),
            melanolib::math::min(enough_words, max_words),
            max_words
        );
    }

    std::map<std::string, melanolib::string::TextGenerator> generators;
    std::size_t max_words = 0;
};

class MarkovServer : public httpony::Server
{
public:
    explicit MarkovServer(
        httpony::IPAddress listen,
        melanolib::time::seconds timeout,
        std::size_t max_size,
        std::string log_format,
        std::string title,
        const MarkovChains& chains
    )
        : Server(listen),
          log_format(std::move(log_format)),
          title(std::move(title)),
          chains(chains)
    {
        if ( timeout.count() > 0 )
            set_timeout(timeout);

        if ( max_size )
            set_max_request_size(max_size);
    }

    ~MarkovServer()
    {
        std::cout << "Server stopped\n";
    }

    void respond(httpony::Request& request, const httpony::Status& status) override
    {
        httpony::Response response = build_response(request, status);
        log_response(log_format, request, response, std::cout);
        send_response(request, response);
    }

private:
    /**
     * \brief Returns a response for the given request
     */
    httpony::Response build_response(
        httpony::Request& request,
        const httpony::Status& status) const
    {
        try
        {
            if ( status.is_error() )
                return simple_response(status, request.protocol);

            if ( request.body.content_length() >= max_request_size() )
                return simple_response(httpony::StatusCode::PayloadTooLarge, request.protocol);

            if ( request.method != "GET" && request.method != "HEAD")
                return simple_response(httpony::StatusCode::MethodNotAllowed, request.protocol);

            if ( request.uri.path.empty() )
                return home(request);
            else if ( request.uri.path.size() <= 2 && request.uri.path[0] == "raw" )
                return api(request);

            return simple_response(httpony::StatusCode::NotFound, request.protocol);
        }
        catch ( const std::exception& error )
        {
            // Create a server error response if an exception happened
            // while handling the request
            std::cerr << error.what() << '\n';
            return simple_response(httpony::StatusCode::InternalServerError, request.protocol);
        }
    }

    httpony::Response home(httpony::Request& request) const
    {
        httpony::Response response(request.protocol);

        using namespace httpony::quick_xml::html;
        using namespace httpony::quick_xml;

        std::string selected = request.uri.query["character"];
        Select select_pony("character");
        select_pony.append(Option("", selected == "", false, Text{"Random"}));
        for ( const auto& item : chains.generators )
            select_pony.append(Option(item.first, selected == item.first));

        HtmlDocument html(title);
        html.body().append(
            Element{"h1", Text{title}},
            Element{"form",
                Element{"p",
                    Label("character", "Character"),
                    std::move(select_pony),
                },
                Element{"p",
                    Label("prompt", "Prompt"),
                    Input("prompt", "text", request.uri.query["prompt"])},
                Element{"p",
                    Label("min-words", "Min words"),
                    Input("min-words", "number",
                          request.uri.query.get("min-words", "5"),
                          Attribute{"min", "0"},
                          Attribute{"max", std::to_string(chains.max_words)}
                    )},
                Element{"p",
                    Label("enough-words", "Enough words"),
                    Input("enough-words", "number",
                          request.uri.query.get("enough-words", "10"),
                          Attribute{"min", "0"},
                          Attribute{"max", std::to_string(chains.max_words)}
                    )},
                Element{"p",
                    Input("submit", "submit", "Chat!"),
                    Input("raw", "submit", "Raw result",
                        Attribute{"onclick", "this.form.action='raw';"})
                }
            }
        );

        if ( request.uri.query.contains("submit") )
            html.body().append(
                Element{"h2", Text{"Output"}},
                Element{"div",
                    Text{generate(request.uri.query["character"], request)}
                }
            );


        response.body.start_output("text/html; charset=utf-8");
        html.print(response.body, true);
        response.body << "\r\n";
        return response;
    }

    httpony::Response api(httpony::Request& request) const
    {
        httpony::Response response(request.protocol);
        std::string character;
        if ( request.uri.path.size() == 2 )
            character = request.uri.path[1];
        else
            character = request.uri.query["character"];
        response.body.start_output("text/plain; charset=utf-8");
        response.body << generate(character, request) << "\r\n";
        return response;

    }


    std::string generate(const std::string& character, httpony::Request& request) const
    {
        return chains.generate(
            character,
            request.uri.query["prompt"],
            melanolib::string::to_uint(request.uri.query["min-words"]),
            melanolib::string::to_uint(request.uri.query["enough-words"])
        );
    }

    /**
     * \brief Creates a simple text response containing just the status message
     */
    httpony::Response simple_response(
        const httpony::Status& status,
        const httpony::Protocol& protocol) const
    {
        httpony::Response response(status, protocol);
        response.body.start_output("text/plain");
        response.body << response.status.message << '\n';
        return response;
    }

    /**
     * \brief Sends the response back to the client
     */
    void send_response(httpony::Request& request,
                       httpony::Response& response) const
    {
        // We are not going to keep the connection open
        if ( response.protocol >= httpony::Protocol::http_1_1 )
        {
            response.headers["Connection"] = "close";
        }

        // Ensure the response isn't cached
        response.headers["Expires"] = "0";

        // This removes the response body when mandated by HTTP
        response.clean_body(request);

        if ( !send(request.connection, response) )
            request.connection.close();
    }

    std::string log_format;
    std::string title;
    const MarkovChains& chains;
};

bool run(const boost::program_options::variables_map& vm)
{
    if ( vm.count("help") )
        return false;

    MarkovChains chains;
    chains.load(vm["graph-path"].as<std::string>());
    chains.max_words = vm["max-words"].as<std::size_t>();
    if ( chains.generators.empty() )
    {
        std::cerr << "No Markov data";
        return false;
    }
    std::cout << "Loaded markov chains:\n";
    for ( const auto& item : chains.generators )
        std::cout << '\t' << item.first << '\n';

    MarkovServer server(
        httpony::IPAddress(vm["listen"].as<std::string>()),
        melanolib::time::seconds(vm["timeout"].as<std::size_t>()),
        vm["max-size"].as<std::size_t>(),
        vm["log-format"].as<std::string>(),
        vm["page-title"].as<std::string>(),
        chains
    );
    server.start();
    std::cout << "Server started on port "<< server.listen_address().port << '\n';
    std::cin.get();
    return true;

}

int main(int argc, char** argv)
{
    using namespace boost::program_options;

    options_description generic_options("Generic Options");
    generic_options.add_options()
        ("help", "Show this help message and exit")
    ;

    options_description http_options("Http Options");
    http_options.add_options()
        ("listen",
         value<std::string>()->default_value("localhost"),
         "IP address (and optional port to listen to).")
        ("timeout",
         value<std::size_t>()->default_value(5),
         "Request timeout (in seconds). 0 for no timeout.")
        ("max-size",
         value<std::size_t>()->default_value(1024*1024),
         "Maximum acceptable request size in bytes, defaults to 1M. 0 for unlimited.")
        ("log-format",
         value<std::string>()->default_value("%h %l %u %t \"%r\" %s %b \"%{Referer}i\" \"%{User-Agent}i\""),
         "Log format (Uses replacements similar to the Apache custom log).")
        /// \todo serve static
        /// \todo ssl support
    ;

    options_description markov_options("Text Generator");
    markov_options.add_options()
        /// \todo multiple paths and single files
        ("graph-path",
         value<std::string>(),
         "Path to scan for Markov data")
        ("max-words",
         value<std::size_t>()->default_value(256),
         "Maximum number of words allowed for the generator.")
    ;

    options_description html_options("Html Options");
    html_options.add_options()
        ("page-title",
         value<std::string>()->default_value("Pony chat generator"),
         "Title for html documents.")
    ;

    options_description all_options;
    all_options.add(generic_options).add(http_options).add(markov_options).add(html_options);

    variables_map vm;
    try
    {
        store(command_line_parser(argc, argv).options(all_options).run(), vm);
        notify(vm);

        if ( run(vm) )
            return 0;
    }
    catch( const error& err )
    {
        std::cerr << "Error: " << err.what() << "\n\n";
    }

    std::cerr << all_options << "\n";
    return 1;

}
