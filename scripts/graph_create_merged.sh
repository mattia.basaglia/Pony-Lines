#!/bin/bash
# Copyright 2016 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
SCRIPTS_PATH="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"
source "$SCRIPTS_PATH/config.sh"

CHARACTER="Anypony"
GRAPH_FILE="$GRAPHS_PATH/$CHARACTER"
DOT_FILE="$GRAPHVIZ_PATH/$CHARACTER.dot"

mkdir -p "$GRAPHVIZ_PATH" "$GRAPHS_PATH"

if ! [ -f "$GENERATOR" ]
then
    (mkdir build && cd build && cmake .. && make)
fi

"$GENERATOR" --fresh --load <(find "$PARSED_TRANSCRIPT_PATH" -type f -print0 | xargs -0 awk -F ::: '{print $2;}') --graph "$GRAPH_FILE" --dot "$DOT_FILE" --stats
