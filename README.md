Pony Markov Text Generator
==========================

This repository contains scripts to strip transcripts from mlp.wikia.com and
parse them to generate Markov chains for various characters.
Those Markov chains can then be used to generate new random text based on the
input data.

License
=======

The files in ./data contain text from mlp.wikia.com and can be re-generated
using the scripts, the contents of the wiki are listed as CC-BY-SA.

The web server (./src/markov_http.cpp) is AGPLv3+.

GPLv3+ for everything else.

Scripts
=======
Follows a list of available scripts (see ./scripts) with a brief description,
listed in a logical order.

config.sh
---------
Not an executable, contains variable definitions used by other scripts.
This is sourced automatically by the scripts that use these variables.
Note that all values declared here can be overridden by the environment.

check_config.sh
---------
Prints the current config variables.

data_to_json.py
---------------
Takes a list of raw transcript file names and prints some JSON with
all of the related data.

get_list.sh
-----------
Retrieves a list of episodes from the wiki and stores it in the appropriate
file properly url-encoded to be suitable for file names and wiki API calls.

get_episode.sh
--------------
Downloads the raw transcript of a single episode ($1).

get_all.sh
----------
Removes existing raw transcripts, then calls get_episode.sh on all the episodes
listed in the file created by get_list.sh.

parse_stream.py
---------------
Parses the contents of a raw wiki transcript (stdin to stdout), formatting it
into a more easily parsable format.

parse_test.sh
-------------
Tests parse_stream.py against a known transcript file containing various
edge cases.

parse_file.sh
-------------
Calls parse_stream.sh on a file specified by name ($1) and store is on
the appropriate location.

parse_all.sh
------------
Removes existing parsed files, then calls parse_file.sh on all raw transcripts.

split_stream.sh
---------------
Splits stdin (which shall be in the format output by parse_stream.sh) into
multiple files, appending lines from the same character to the appropriate file.

split_file.sh
-------------
Calls split_stream.sh on a file given by name ($1).

split_all.sh
------------
Removes existing character line files, then splits all parsed transcripts
using split_file.sh.

pony_names.sh
-------------
Scans the files created by split_stream.sh to display character names,
filtering out groups, generic names, and duplicates.

graph_create.sh
---------------
Calls the compiled binary to generate Markov chains from the output from
split_stream.sh.
The correct file is selected based on the character name ($1).

scripts/graph_create_merged.sh
------------------------------
Creates a graph called "Anypony" containing all of the dialog data.
Warning: The generated text is not as funny as for character-specific graphs
and it takes longer to generate.

graph_create_all.sh
-------------------
Calls graph_create.sh for all characters configured in config.sh.

graph_visualize.sh
------------------
Generates an SVG image from a dot files created by graph_create.sh.
The correct file is selected based on the character name ($1).

chat.sh
-------
Uses the data from graph_create.sh to generate random text.
It accepts a character name ($1) and forwards other command line options to
the compiled binary (see ./build/bin/graphgen --help for more info)

Binaries
========

Binaries are compiled using the CMake build system

graphgen
--------
Generates Markov chains from the input lines and random text from the chains.
