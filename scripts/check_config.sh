#!/bin/bash
# Copyright 2016 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
SCRIPTS_PATH="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"

source "$SCRIPTS_PATH/config.sh"

vars=(
    ROOT
    DATA_PATH
    GRAPHS_PATH
    RAW_TRANSCRIPT_PATH
    PARSED_TRANSCRIPT_PATH
    LINES_PATH
    GRAPHVIZ_PATH
    EPISODE_LIST_FILE
    BUILD_PATH
    GENERATOR
    SEASONS
    CHARACTERS
)

for var in "${vars[@]}"
do
    if var_type "$var" array
    then
        echo "$var :"
        var="$var[@]"
        for val in "${!var}"
        do
            echo " : $val"
        done
    else
        echo "$var : ${!var}"
    fi
done
