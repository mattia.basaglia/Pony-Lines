#!/usr/bin/python
# Copyright 2016 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import re

# Wikitext comment regex
comment = re.compile(r"<!--[^-]*-->")
# Wikitext inline note markup
note = re.compile(r"\{\{H:title\|[^|]\|([^}]+)\}\}")
# Pattern to collapse spaces and replace weird characters
spaces = re.compile(r"\s+", re.UNICODE)

def clean_raw(line):
    """
    Removes some problematic markup from a line of wikitext
    """
    return spaces.sub(' ',
        note.sub('\1',
            comment.sub('', line[:-1])
        ).decode('utf-8')
    ).encode('utf-8')


# Regex pattern for markup to be removed from a line of dialog
line_markup = re.compile(r"''+|\[[^]]*\]|\{\{[^\}]*\}\}")

def clean_line(line):
    """
    Removes markup from wikitext containing a line of dialog
    """
    return line_markup.sub('', line).lstrip(':').strip()


# Regex pattern to remove wiki markup from a list of names
name_clean = re.compile(r"'''|\{\{[^}]+\||\}\}")

def clean_name(name):
    """
    Removes markup from wikitext containing a list of names
    """
    return name_clean.sub('',
        name.translate(None, "[]\"")
    ).strip()


# Caches the last name list to be used when lines don't repeat character names
names = []
# Regex pattern to extract names from
name_pattern = re.compile(r"^:+((?:'''|\{\{[^#]:?)(?:H:|[^:])+)(?:\s*$|:)")
# Regex pattern to split lists of names
split_names = re.compile(r",?\s+and\s+|,\s*")

def split_line(line):
    """
    Splits a raw wikitext line into a pair of (names, plaintext)
    """
    global names
    line = clean_raw(line)
    name_match = name_pattern.match(line)
    if name_match:
        split = name_match.span(0)[1]
        name = clean_name(name_match.group(name_match.lastindex))
        names = filter(bool, split_names.split(name))
        line = line[split:]
    return names, clean_line(line)


def lines(source):
    """!
    \brief Generator of line data
    \param source Iterable that yields strings (one per line)
    \returns A generator, each element is a pair where the first element is
    a list of character names, and the second is a line in plaintext
    """
    for line in source:
        if line and line[0] == ':':
            names, quote = split_line(line)
            if names and quote:
                yield names, quote
