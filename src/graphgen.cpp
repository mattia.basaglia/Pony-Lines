/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <iostream>
#include <fstream>
#include <boost/program_options.hpp>
#include "melanolib/string/text_generator.hpp"

int main(int argc, char** argv)
{
    using namespace boost::program_options;

    options_description options("Generic Options");
    options.add_options()
        ("help", "Show this help message and exit")
    ;

    options_description input_args("Input");
    input_args.add_options()
        ("load", value<std::string>(), "Input text file to add to the graph")
        ("text", value<std::string>(), "Input text to add to the graph")
    ;

    options_description output_args("Output");
    output_args.add_options()
        ("dot", value<std::string>(), "Dot file to visualize the graph")
        ("stats", "Displays some stats about the graph")
        ("generate", "Generates some text")
    ;

    options_description graph_options("Graph Options");
    graph_options.add_options()
        ("graph", value<std::string>(), "Graph storage file name")
        ("fresh", "Whether to start afresh (ie: discard the existing graph file), only relevant with one of the input options")
        ("max-graph-size",
         value<std::size_t>()->default_value(std::numeric_limits<std::size_t>::max()),
         "Maximum number of words to keep")
        ("max-age",
         value<std::size_t>()->default_value(60),
         "Age cutoff for when max-words is exceeded (in days)")
    ;

    options_description textgen("Text Generation");
    textgen.add_options()
        ("min-words",
         value<std::size_t>()->default_value(4),
         "Minimum number of words to generate (hint)")
        ("enough-words",
         value<std::size_t>()->default_value(10),
         "Number of words after which the sentence should end (hint)")
        ("max-words",
         value<std::size_t>()->default_value(128),
         "maximum number of words to generate (hard limit)")
        ("prompt",
         value<std::string>()->default_value(""),
         "Text that should be contained on the result")
    ;

    options_description all_options;
    all_options.add(options).add(input_args).add(output_args).add(graph_options).add(textgen);

    variables_map vm;
    try
    {
        store(command_line_parser(argc, argv).options(all_options).run(), vm);
        notify(vm);

        if ( !vm.count("help") )
        {
            using melanolib::string::TextGenerator;
            TextGenerator generator(
                vm["max-graph-size"].as<std::size_t>(),
                melanolib::time::days(vm["max-age"].as<std::size_t>())
            );

            if ( vm.count("graph") && !vm.count("fresh") )
            {
                std::clog << "Loading " << vm["graph"].as<std::string>() << '\n';
                std::ifstream input(vm["graph"].as<std::string>());
                if ( input.is_open() )
                    generator.load(input);
            }

            if ( vm.count("load") || vm.count("text") )
            {
                if ( vm.count("load") )
                {
                    std::clog << "Processing " << vm["load"].as<std::string>() << '\n';
                    generator.add_text(std::ifstream(vm["load"].as<std::string>()));
                }

                if ( vm.count("text") )
                {
                    std::clog << "Adding text\n";
                    generator.add_text(vm["text"].as<std::string>());
                }

                if ( vm.count("graph") )
                {
                    std::clog << "Saving " << vm["graph"].as<std::string>() << '\n';
                    std::ofstream output(vm["graph"].as<std::string>());
                    generator.store(output);
                }
            }

            if ( vm.count("dot") )
            {
                    std::clog << "Creating " << vm["dot"].as<std::string>() << '\n';
                std::ofstream output(vm["dot"].as<std::string>());
                generator.store(output, TextGenerator::StorageFormat::Dot);
            }

            if ( vm.count("stats") )
            {
                TextGenerator::Stats stats = generator.stats();
                std::cout << stats.word_count << " words\n";
                std::cout << stats.transitions << " transitions\n";
                std::cout << "Most common word: " << stats.most_common << "\n";
            }

            if ( vm.count("generate") )
            {
                std::cout << generator.generate_string(
                    vm["prompt"].as<std::string>(),
                    vm["min-words"].as<std::size_t>(),
                    vm["enough-words"].as<std::size_t>(),
                    vm["max-words"].as<std::size_t>()
                ) << '\n';
            }

            return 0;
        }
    }
    catch( const error& err )
    {
        std::cerr << "Error: " << err.what() << "\n\n";
    }

    std::cerr << all_options << "\n";
    return 1;

}
