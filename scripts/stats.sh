#!/bin/bash
# Copyright 2016 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
SCRIPTS_PATH="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"
source "$SCRIPTS_PATH/config.sh"

TOP_N=10

function topify()
{
    local pattern="$1"
    [ "$pattern" ] || pattern='s/\s*([0-9]+)\s*(.*)/\2\t\1/'

    sort -nr | head -n "$TOP_N" | sed -r "$pattern" | column -t -s $'\t'
}

echo "Totals:"
echo "$(ls -1A "$LINES_PATH" | wc -l) ponies"
echo "$(cat "$LINES_PATH"/* | wc -l) lines"
echo "$(cat "$LINES_PATH"/* | wc -w) words"
echo

echo "Top $TOP_N most talkative (lines):"
wc -l "$LINES_PATH"/* | head -n -1 | topify 's~\s*([0-9]+) .*/(.*)$~\2\t\1 lines~'
echo

echo "Top $TOP_N most talkative (words):"
wc -w "$LINES_PATH"/* | head -n -1 | topify 's~\s*([0-9]+) .*/(.*)$~\2\t\1 words~'
echo

echo "Top $TOP_N most common lines:"
cat "$LINES_PATH"/* | sort | uniq -c | topify
echo

