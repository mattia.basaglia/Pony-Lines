#!/bin/bash
# Copyright 2016 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
SCRIPTS_PATH="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"
source "$SCRIPTS_PATH/config.sh"

API_BASE="http://mlp.wikia.com/api.php?format=txt&action=query&list=categorymembers&cmlimit=64&cmtitle=Category:"

pc_char=(   '!'  '#'  '\$' '&'  "'" '\\(' ')'  '\*' '+'  ','  '\/' ':'  ';'  '='  '\?' '@'  '\[' ']')
pc_percent=("21" "23" "24" "26" "27" "28" "29" "2A" "2B" "2C" "2F" "3A" "3B" "3D" "3F" "40" "5B" "5D")
pc_patterns=()
pc_count="${#pc_char[@]}"
for (( i=0; i<pc_count; i++ ))
do
    pc_patterns+=("-e" "s/${pc_char[i]}/%${pc_percent[i]}/g")
done

function percent_encode()
{
    sed "${pc_patterns[@]}"
}

rm -f "$EPISODE_LIST_FILE"
for season in "${SEASONS[@]}"
do
    curl -sS "${API_BASE}Season_${season}_transcripts" | sed -ne '/title/{s/\s*\[title\] => Transcripts\///;s/ /_/g;p}' | percent_encode >>"$EPISODE_LIST_FILE"
done
